﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer
{
    void Interact(InteractableObject interactable, HandController controller);
    void Break(InteractableObject interactable);
}
