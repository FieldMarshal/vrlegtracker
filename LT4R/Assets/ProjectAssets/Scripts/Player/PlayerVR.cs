﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.XR;

[System.Serializable] // Generic Event holding button value
public class AButtonEvent : UnityEvent<bool>
{
    public bool Value { get; set; }

    public void Initialize(bool value, UnityAction<bool> method)
    {
        Value = value;
        AddListener(method);
    }
}


public class PlayerVR : MonoBehaviour, IPlayer
{
    public GameObject LeftAnchor;
    public GameObject RightAnchor;

    private HandController _leftController;
    private HandController _rightController;

    private UnityEngine.XR.InputDevice _leftDevice;
    private UnityEngine.XR.InputDevice _rightDevice;
 

    // Start is called before the first frame update
    void Start()
    {
        SetDevices();

        //Initialize Hands
        _leftController = LeftAnchor.AddComponent<HandController>().InitializeHandController(this);
        _rightController = RightAnchor.AddComponent<HandController>().InitializeHandController(this);

    }

    // Update is called once per frame
    private new void Update()
    {

        //Set Tracked Devices
        SetDevicePosAndRot(XRNode.LeftHand, LeftAnchor);
        SetDevicePosAndRot(XRNode.RightHand, RightAnchor);

        //Set Buttons
        UpdateButtonState(_leftDevice, CommonUsages.gripButton, _leftController.GripEvent);
        UpdateButtonState(_rightDevice, CommonUsages.gripButton, _rightController.GripEvent);

        UpdateButtonState(_leftDevice, CommonUsages.primary2DAxisClick, _leftController.ClickEvent);
        UpdateButtonState(_rightDevice, CommonUsages.primary2DAxisClick, _rightController.ClickEvent);

        UpdateButtonState(_leftDevice, CommonUsages.triggerButton, _leftController.TriggerEvent);
        UpdateButtonState(_rightDevice, CommonUsages.triggerButton, _rightController.TriggerEvent);

        UpdateButtonState(_leftDevice, CommonUsages.menuButton, _leftController.MenuEvent);
        UpdateButtonState(_rightDevice, CommonUsages.menuButton, _rightController.MenuEvent);
    }

    private static void SetDevicePosAndRot(XRNode trackedDevice, GameObject anchor)
    {
        anchor.transform.localPosition = UnityEngine.XR.InputTracking.GetLocalPosition(trackedDevice);
        anchor.transform.localRotation = UnityEngine.XR.InputTracking.GetLocalRotation(trackedDevice);
    }

    private static InputDevice GetCurrentDevice(XRNode node)
    {
        var device = new InputDevice();
        var devices = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevicesAtXRNode(node,
            devices);
        if (devices.Count == 1)
        {
            device = devices[0];
            //Debug.Log($"Device name '{device.name}' with role '{device.role.ToString()}'");
        }
        else if (devices.Count > 1)
        {
            Debug.Log($"Found more than one '{device.role.ToString()}'!");
            device = devices[0];
        }

        return device;
    }

    private void UpdateButtonState(InputDevice device, InputFeatureUsage<bool> button,
        AButtonEvent aButtonPressEvent)
    {
        bool tempState;
        bool invalidDeviceFound = false;
        bool buttonState = false;

        tempState = device.isValid // the device is still valid
                    && device.TryGetFeatureValue(button, out buttonState) // did get a value
                    && buttonState; // the value we got

        if (!device.isValid)
                invalidDeviceFound = true;

        if (tempState != aButtonPressEvent.Value) // Button state changed since last frame
        {
            aButtonPressEvent.Invoke(tempState);
            aButtonPressEvent.Value = tempState;
        }

        if (invalidDeviceFound) // refresh device lists
           SetDevices();
    }

    private void SetDevices()
    {
        //Set Controller Devices
        _leftDevice = GetCurrentDevice(XRNode.LeftHand);
        _rightDevice = GetCurrentDevice(XRNode.RightHand);
    }

    //Debug Device
    private void ShowCurrentlyAvailableXRDevices()
    {
        var inputDevices = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevices(inputDevices);
        foreach (var device in inputDevices)
        {
            Debug.Log($"Device found with name '{device.name}' and role '{device.role.ToString()}'");
        }
    }

    public void Interact(InteractableObject interactable, HandController controller)
    {
        if(!controller.GrabbedObject && controller.IsGripPressed)
            controller.Grab(interactable);
    }

    public void Break(InteractableObject interactable)
    {
        Destroy(interactable.gameObject);
    }
}
