﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[RequireComponent(typeof(Animator), typeof(Rigidbody))]
public class HandController : MonoBehaviour
{
    public IPlayer Owner;

    //Parts
    public Transform AttachPoint;
    public InteractableObject GrabbedObject;


    //Button State
    public bool IsGripPressed;
    public bool IsTriggerPressed;
    public bool IsMenuPressed;
    public bool IsClickPressed;

    // Button Events
    public AButtonEvent GripEvent { get; set; }
    public AButtonEvent TriggerEvent { get; set; }
    public AButtonEvent MenuEvent { get; set; }
    public AButtonEvent ClickEvent { get; set; }

    private Animator _animator;

    private Vector3 _positionBeforeLast;
    private Vector3 _lastPosition;

    //Throw
    public GameObject prefab;
    public Rigidbody attachPoint; //TODO Rename this

    FixedJoint joint;

    private Menu playerMenu;

    void Start()
    {
        _animator = GetComponent<Animator>();
        AttachPoint = transform.GetChild(0); //temporary TODO remove this.
        attachPoint = GetComponent<Rigidbody>();

        playerMenu = FindObjectOfType<Menu>();
        Debug.Log(" Found Object: " + playerMenu.gameObject.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (IsGripPressed && GrabbedObject)
        {
            if(_lastPosition == Vector3.zero)
            InvokeRepeating("SetLastPosition", 1.0f, 1.0f);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        InteractableObject item = other.GetComponent<InteractableObject>();
        if (item.IsBreakable)
            Owner.Break(item);
    }

    void OnTriggerStay(Collider other)
    {
        InteractableObject item = other.GetComponent<InteractableObject>();
        if (item)
            Owner.Interact(item, this);
    }

    public HandController InitializeHandController(IPlayer currentOwner)
    {
        InitializeButtonInput();

        Owner = currentOwner;
        return this;
    }

    private void InitializeButtonInput()
    {
        (GripEvent = new AButtonEvent()).Initialize(IsGripPressed, OnGripButtonEvent);
        (TriggerEvent = new AButtonEvent()).Initialize(IsTriggerPressed, OnTriggerButtonEvent);
        (MenuEvent = new AButtonEvent()).Initialize(IsMenuPressed, OnMenuButtonEvent);
        (ClickEvent = new AButtonEvent()).Initialize(IsClickPressed, OnClickButtonEvent);

    }

    // Button Functions
    private void OnGripButtonEvent(bool pressed)
    {
        IsGripPressed = pressed;
        _animator.SetBool("isGrabbing", pressed);

        if (pressed)
        {
            Application.LoadLevel(Application.loadedLevel);
            Debug.Log("Grip Pressed");
            //Owner.
        }
        else
        {
            Debug.Log("Grip Released");
            if(GrabbedObject)
                Drop();
        }
    }

    private void OnTriggerButtonEvent(bool pressed)
    {
        IsTriggerPressed = pressed;
        _animator.SetBool("isPunching", pressed);

        if (pressed)
        {
            Debug.Log("Trigger Pressed");
            playerMenu.Continue();
        }
        else
        {
            Debug.Log("Trigger Released");
        }
    }

    private void OnMenuButtonEvent(bool pressed)
    {
        IsMenuPressed = pressed;
        if (pressed)
        {
            Debug.Log("Menu Pressed");
        }
    }

    private void OnClickButtonEvent(bool pressed)
    {
        IsClickPressed = pressed;
        _animator.SetBool("isPointing", pressed);

        if (pressed)
        {
            Debug.Log("Click Pressed");
        }
        else
        {
            Debug.Log("Click Released");
        }
    }

    public void Grab(InteractableObject interactable)
    {
        joint = interactable.gameObject.AddComponent<FixedJoint>();
        joint.connectedBody = attachPoint;

        interactable.transform.parent = AttachPoint;
        interactable.SetAttached();
        GrabbedObject = interactable;
    }

    public void Drop()
    {

        GrabbedObject.SetDetached(GetForce());
        GrabbedObject = null;

        _lastPosition = Vector3.zero;
        CancelInvoke();
    }

    private void SetLastPosition()
    {
        _positionBeforeLast = _lastPosition;
        _lastPosition = transform.position;
    }

    private Vector3 GetDir(Vector3 point1, Vector3 point2)
    {
        return (point1 - point2).normalized;
    }

    private Vector3 GetVelocity()
    {
        return new Vector3();
    }

    private Vector3 GetForce()
    {
        Vector3 point1 = _lastPosition;
        Vector3 point2 = transform.position;

        float dis = Vector3.Distance(point1, point2);
        if (dis < 1f)
        {
            dis = Vector3.Distance(_positionBeforeLast, point2);
        }

        return GetDir(point1, point2) * ( dis / (GrabbedObject.ItemWeight * 0.1f));
    }
}
