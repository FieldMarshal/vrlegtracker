﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Menu : MonoBehaviour
{
    private TextMeshPro currentContent;
    private int currentPageindex;

    public GuideSpawn guideSpawn;

    private bool isLoading;

    public GameObject Background;

    public List<GameObject> Pages;

    Color lerpedColor = Color.blue;

    private Material backgroundMaterial;

    // Start is called before the first frame update
    void Start()
    {
        currentContent = Pages[0].GetComponent<TextMeshPro>();

        foreach (GameObject page in Pages)
        {
            page.SetActive(false);
        }

        Pages[currentPageindex].SetActive(true);

        Renderer rend = Background.GetComponent<Renderer>();
        backgroundMaterial = new Material(rend.sharedMaterial);

        //Continue();
    }

    // Update is called once per frame
    void Update()
    {
        //lerpedColor = Color.Lerp(Color.white, Color.blue, Mathf.PingPong(Time.time, 1));

        //Set the main Color of the Material to lerpedColor
        //backgroundMaterial.SetColor("_BaseColor", lerpedColor);
    }

    public void Continue()
    {
        if (!isLoading)
        {

            guideSpawn.showNextGuide();

            GetComponent<AudioSource>().Play(0);

            Pages[currentPageindex].SetActive(false);

            currentPageindex %= Pages.Count;

            Pages[currentPageindex+1].SetActive(true);

            currentPageindex++;

        }

        StartCoroutine(loadContinue());
    }

    private IEnumerator loadContinue()
    {
        isLoading = true;
        yield return new WaitForSeconds(3);
        isLoading = false;
    }
}
