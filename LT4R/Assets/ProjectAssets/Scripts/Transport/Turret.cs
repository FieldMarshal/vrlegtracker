﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public TurretShootingPoint[] TurretShootingPoints;

    private void OnEnable()
    {
        TurretShootingPoints = GetComponentsInChildren<TurretShootingPoint>();
        foreach (TurretShootingPoint point in TurretShootingPoints)
        {
            point.Owner = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
