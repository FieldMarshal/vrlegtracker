﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargePort : MonoBehaviour
{
    public Transport Owner;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        InteractableObject item = other.GetComponent<InteractableObject>();
        if (item && Owner)
        {
            Owner.Charge(item.ItemValue);
        }
    }
}
