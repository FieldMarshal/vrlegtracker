﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transport : MonoBehaviour
{
    public Transform RacerAttachPoint;
    public Transform Platform;
    public ChargePort[] ChargePorts;

    private Quaternion _intialRotation;

    private float _currentCharge;
    private const float MaxCharge = 100;

    private void OnEnable()
    {
        ChargePorts = GetComponentsInChildren<ChargePort>();
        foreach (ChargePort port in ChargePorts)
        {
            port.Owner = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _intialRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        Platform.rotation = Quaternion.identity;
    }

    public void Charge(float val)
    {
        _currentCharge += val;
    }
}
