﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideSpawn : MonoBehaviour
{
    public GameObject[] guidePrefabs;

    private int lastGuideIndex;

    public Transform spawnLocation;
    
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject guide in guidePrefabs)
        {
            var guideballs = guide.GetComponent<Guide>().guideBalls;

            foreach (GameObject guideBall in guideballs)
            {
                guideBall.SetActive(false);
            }
        }

        spawnLocation = transform;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void showNextGuide()
    {
        StartCoroutine(displayGuide());
    }

    public IEnumerator displayGuide()
    {
        Instantiate(guidePrefabs[lastGuideIndex], spawnLocation.position, Quaternion.identity);

        var guideballs = guidePrefabs[lastGuideIndex].GetComponent<Guide>().guideBalls;

        foreach (GameObject guideBall in guideballs)
        {
            yield return new WaitForSeconds(2);

            GetComponent<AudioSource>().Play(0);
            guideBall.SetActive(false);
        }

        lastGuideIndex++;
    }
}
