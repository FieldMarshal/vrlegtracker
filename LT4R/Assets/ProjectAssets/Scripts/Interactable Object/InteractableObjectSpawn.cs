﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public struct SpawnPoint
{
    public GameObject SpawnLocation { get; set; }
    public bool IsOccupied;

    public SpawnPoint(bool occupied)
    {
        SpawnLocation = new GameObject("SpawnPoint");
        IsOccupied = occupied;
    }
}

[RequireComponent(typeof(BoxCollider))]
public class InteractableObjectSpawn : MonoBehaviour
{
    [Header("**Change the Box Collider Size on the z axis" +
            "to change spawn length**")]
    public int _spawnPointCount = 4;

    public List<SpawnPoint> SpawnPoints;
    public GameObject[] Specials;
    public GameObject[] InteractableGameObjectPrefabs;


    // Start is called before the first frame update
    void Start()
    {
        SpawnPoints = new List<SpawnPoint>();
        float spawnDistance = GetComponent<BoxCollider>().size.z / _spawnPointCount;
        Vector3 dir = transform.forward;
        Vector3 offsetPos = dir * (GetComponent<BoxCollider>().size.z / 2);
        Vector3 currentPos = transform.position + offsetPos;

        for (int i = 0; i < _spawnPointCount; i++)
        {
            currentPos -= dir * spawnDistance;
            SpawnPoints.Add(new SpawnPoint(false));
            SpawnPoints[i].SpawnLocation.transform.parent = transform;
            SpawnPoints[i].SpawnLocation.transform.position = currentPos;
        }

        SpawnObjects();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnObjects()
    {
        for (int i = SpawnPoints.Count - 1; i >= 0; i--)
        {
            SpawnPoint point = SpawnPoints[i];
            if(point.IsOccupied) continue;

            GenerateNextInteractObj(point.SpawnLocation.transform);

            point.IsOccupied = (point.SpawnLocation.transform.childCount == 0);
        }
    }

    public void GenerateNextInteractObj(Transform spawnTransform)
    {
        var obj = Instantiate(InteractableGameObjectPrefabs[Random.Range(0,
            InteractableGameObjectPrefabs.Length)], spawnTransform.position, spawnTransform.rotation);
        var interactableObject = obj.GetComponent<InteractableObject>();

        obj.transform.parent = spawnTransform;
        interactableObject.InitializeTransform();

        if(Specials.Length > 0)
        if (Random.Range(0, 100) % 5 == 0)
        {
            interactableObject.Special = Specials[Random.Range(0, Specials.Length)];
        }

        float itemValue = 1f / Random.Range(0, 10);
        interactableObject.ItemValue = itemValue;
        interactableObject.IsAttachable = true;
        float ItemWeight = itemValue * Random.Range(1, 100);
        interactableObject.ItemWeight = ItemWeight;
        interactableObject.SetScale(ItemWeight);
        interactableObject.ChangeLook();

    }
}
