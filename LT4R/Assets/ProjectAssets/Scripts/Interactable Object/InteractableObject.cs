﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SocialPlatforms;

[RequireComponent(typeof(Collider), typeof(AudioSource))]
public class InteractableObject : MonoBehaviour
{
    public bool IsBreakable;
    public bool IsAttachable;

    public float ItemWeight;
    public float ItemValue;

    public GameObject Special;

    [Range(1f, 5f)]
    public float SpecialSpawnOffset;
    public float SpecialLifeTime; //Time the special ability will last for (20s)


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Break()
    {
        GetComponent<AudioSource>().Play(0);
        gameObject.SetActive(false);
        StartCoroutine(TimedDestroy());
    }

    private IEnumerator TimedDestroy()
    {
        yield return new WaitForSeconds(5);

        Destroy(gameObject);
    }

    public void SetAttached()
    {
        ShowSpecial();
        InitializeTransform();
    }

    public void SetDetached(Vector3 force)
    {
        //Rigidbody rigid = gameObject.AddComponent<Rigidbody>();
        //rigid.AddForce(force, ForceMode.Acceleration);
        transform.parent = null;
    }

    public void InitializeTransform()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

    public void SetScale(float itemWeight)
    {

    }

    public void ChangeLook()
    {

    }

    private IEnumerable ShowSpecial()
    {
        if (!Special) yield return null;

        GameObject special = Instantiate(Special, transform.position + Vector3.up * SpecialSpawnOffset,
            Quaternion.identity);
        Special = null;
        //Special will only live 1/5 the time of its ability life time.
        yield return new WaitForSeconds(SpecialLifeTime/5);
        special.GetComponent<InteractableObject>().Break();
    }



}
