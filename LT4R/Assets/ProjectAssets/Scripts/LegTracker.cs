﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegTracker : MonoBehaviour
{
    public TrackerParser TrackingDataParser;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(TrackingDataParser.Rotation);
        //transform.position = transform.position + TrackingDataParser.Acceleration.normalized;
    }
}
