﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class TrackerParser : MonoBehaviour
{
    // Debug
    public string Message;
    public bool isDebugging = false;

    //Data
    public Vector3 Rotation;
    public Vector3 Acceleration;

    private TextMeshPro DebugText;
    private int parseCounter;
    private string receivedMessage;
    private string connectionMessage;


    // Start is called before the first frame update
    void Start()
    {
        Rotation = new Vector3(0.0f, 0.0f, 0.0f);
        Acceleration = new Vector3(0.0f, 0.0f, 0.0f);

        //Debug
        DebugText = GetComponent<TextMeshPro>();

        //Test
        if(isDebugging)
        ParseString(Message);

    }

    // Update is called once per frame
    void Update()
    {
        DebugText.text = connectionMessage + "\n Acceleration: " + Acceleration.ToString() +
                         "\n Rotation" + Rotation.ToString() + receivedMessage;
    }

    public void ParseString(string msg)
    {
        parseCounter++;
        receivedMessage = "\n Called " + parseCounter  + " times with length " +
                          msg.Length + "\n" + msg; // Visual Debug

        string[] words = msg.Split('|');

        foreach (string word in words)
        {
            if (word.Contains("Gy"))
            {
                if (word.Contains("X"))
                    Rotation.x = GetNumDataFromString(word);
                else if (word.Contains("Y"))
                    Rotation.y = GetNumDataFromString(word);
                else if (word.Contains("Z"))
                    Rotation.z = GetNumDataFromString(word);
            }
            else if(word.Contains("Ac"))
            {
                if (word.Contains("X"))
                    Acceleration.x = GetNumDataFromString(word);
                else if (word.Contains("Y"))
                    Acceleration.y = GetNumDataFromString(word);
                else if (word.Contains("Z"))
                    Acceleration.z = GetNumDataFromString(word);
            }

        }


    }

    float GetNumDataFromString(string text)
    {
        string[] words = text.Split('=');
        string textValue = words[words.Length - 1].Replace(" ", "");
        float num = float.Parse(textValue);
        return num;
    }

    public void isConnected()
    {
        connectionMessage = " Connected!!!";
    }

    public void isDisconnected()
    {
        connectionMessage = " Disconnected!!!";
    }

}
